from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem
import random

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1050, 750)
        MainWindow.setWindowIcon(QtGui.QIcon("logo.png"))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout.setObjectName("verticalLayout")
        self.Matrix = QtWidgets.QTableWidget(self.centralwidget)
        self.Matrix.setObjectName("Matrix")
        self.Matrix.setColumnCount(0)
        self.Matrix.setRowCount(0)
        self.verticalLayout.addWidget(self.Matrix)
        self.Protocol = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.Protocol.setObjectName("Protocol")
        self.verticalLayout.addWidget(self.Protocol)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.VertexNum = QtWidgets.QLineEdit(self.centralwidget)
        self.VertexNum.setObjectName("VertexNum")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.VertexNum)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.StartVertex = QtWidgets.QLineEdit(self.centralwidget)
        self.StartVertex.setObjectName("StartVertex")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.StartVertex)

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.MinTemperature = QtWidgets.QLineEdit(self.centralwidget)
        self.MinTemperature.setObjectName("MinTemperature")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.MinTemperature)

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.StepTemperature = QtWidgets.QLineEdit(self.centralwidget)
        self.StepTemperature.setObjectName("StepTemperature")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.StepTemperature)

        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.Q = QtWidgets.QLineEdit(self.centralwidget)
        self.Q.setObjectName("Q")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.Q)

        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.Ants = QtWidgets.QLineEdit(self.centralwidget)
        self.Ants.setObjectName("Ants")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.Ants)

        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_7)
        self.Alpha = QtWidgets.QLineEdit(self.centralwidget)
        self.Alpha.setObjectName("Alpha")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.Alpha)

        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.LabelRole, self.label_8)
        self.Beta = QtWidgets.QLineEdit(self.centralwidget)
        self.Beta.setObjectName("Beta")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.Beta)

        self.Methods = QtWidgets.QComboBox(self.centralwidget)
        self.Methods.setCurrentText("")
        self.Methods.setObjectName("Methods")
        self.formLayout.setWidget(8, QtWidgets.QFormLayout.SpanningRole, self.Methods)
        self.verticalLayout_2.addLayout(self.formLayout)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.CreateMatrix = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(200)
        sizePolicy.setHeightForWidth(self.CreateMatrix.sizePolicy().hasHeightForWidth())
        self.CreateMatrix.setSizePolicy(sizePolicy)
        self.CreateMatrix.setBaseSize(QtCore.QSize(0, 0))
        self.CreateMatrix.setObjectName("CreateMatrix")
        self.verticalLayout_3.addWidget(self.CreateMatrix)

        self.Run = QtWidgets.QPushButton(self.centralwidget)
        self.Run.setObjectName("Run")
        self.verticalLayout_3.addWidget(self.Run)
        self.ClearProtocol = QtWidgets.QPushButton(self.centralwidget)
        self.ClearProtocol.setObjectName("ClearProtocol")
        self.verticalLayout_3.addWidget(self.ClearProtocol)
        self.Close = QtWidgets.QPushButton(self.centralwidget)
        self.Close.setObjectName("Close")
        self.verticalLayout_3.addWidget(self.Close)
        self.verticalLayout_2.addLayout(self.verticalLayout_3)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.setupAddUiConfig()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Поиск кратчайшего пути в графе"))
        self.label.setText(_translate("MainWindow", "Введите количество вершин:"))
        self.label_2.setText(_translate("MainWindow", "Введите начальную вершину:"))
        self.label_3.setText(_translate("MainWindow", "Введите минимальную температуру:"))
        self.label_4.setText(_translate("MainWindow", "Введите шаг уменьшения температуры:"))
        self.label_5.setText(_translate("MainWindow", "Введите Q:"))
        self.label_6.setText(_translate("MainWindow", "Введите количество муравьев:"))
        self.label_7.setText(_translate("MainWindow", "Введите Alpha:"))
        self.label_8.setText(_translate("MainWindow", "Введите Beta:"))
        self.CreateMatrix.setText(_translate("MainWindow", "Создать матрицу"))

        self.Run.setText(_translate("MainWindow", "Найти кратчайший путь"))
        self.ClearProtocol.setText(_translate("MainWindow", "Очистить протокол"))
        self.Close.setText(_translate("MainWindow", "Закрыть программу"))

    def setupAddUiConfig(self):
        self.Methods.addItems(["Метод ближайшего соседа", "Метод имитации отжига", "Муравьиный алгоритм"])
        intValidator = QtGui.QIntValidator()
        self.VertexNum.setValidator(intValidator)  # устанавливаем возможность записывать в VertexNum только чисел
        self.VertexNum.setText("1")
        self.MinTemperature.setValidator(intValidator)
        self.MinTemperature.setText("1")
        self.Q.setValidator(intValidator)
        self.Q.setText("1")
        self.Ants.setValidator(intValidator)
        self.Ants.setText("1")
        self.Alpha.setValidator(intValidator)
        self.Alpha.setText("1")
        self.Beta.setValidator(intValidator)
        self.Beta.setText("1")

        doubleValidator = QtGui.QDoubleValidator()
        self.StepTemperature.setValidator(doubleValidator)
        self.StepTemperature.setText("0.1")

        regex = QtCore.QRegExp("[A-Z]")
        charValidator = QtGui.QRegExpValidator(regex)
        self.StartVertex.setValidator(charValidator)  # устанавливаем возможность записывать в StartVertex только одной буквы
        self.StartVertex.setText("A")

        self.vertexCount = None  # количество вершин
        self.matrixModel = None  # матрица смежности
        self.matrixIsCreate = False

        self.CreateMatrix.clicked.connect(self.CreateMatrixMethod)
        self.Run.clicked.connect(self.RunMethod)
        self.ClearProtocol.clicked.connect(self.ClearProtocolMethod)
        self.Close.clicked.connect(QtCore.QCoreApplication.instance().quit)
        self.Methods.currentIndexChanged.connect(self.ChangeMethod)

        self.MinTemperature.setEnabled(False)
        self.StepTemperature.setEnabled(False)
        self.Q.setEnabled(False)
        self.Ants.setEnabled(False)
        self.Alpha.setEnabled(False)
        self.Beta.setEnabled(False)

    def ChangeMethod(self):
        if self.Methods.currentIndex() == 0:
            self.StartVertex.setEnabled(True)
            self.MinTemperature.setEnabled(False)
            self.StepTemperature.setEnabled(False)
            self.Q.setEnabled(False)
            self.Ants.setEnabled(False)
            self.Alpha.setEnabled(False)
            self.Beta.setEnabled(False)
        elif self.Methods.currentIndex() == 1:
            self.StartVertex.setEnabled(False)
            self.MinTemperature.setEnabled(True)
            self.StepTemperature.setEnabled(True)
            self.Q.setEnabled(False)
            self.Ants.setEnabled(False)
            self.Alpha.setEnabled(False)
            self.Beta.setEnabled(False)
        elif self.Methods.currentIndex() == 2:
            self.StartVertex.setEnabled(True)
            self.MinTemperature.setEnabled(False)
            self.StepTemperature.setEnabled(False)
            self.Q.setEnabled(True)
            self.Ants.setEnabled(True)
            self.Alpha.setEnabled(True)
            self.Beta.setEnabled(True)

    def CreateMatrixMethod(self):
        if self.VertexNum.text() != "":
            self.vertexCount = int(self.VertexNum.text())
            if self.vertexCount > 26 or self.vertexCount < 1:
                self.statusbar.showMessage("Количество вершин должно находиться в диапазоне от 1 до 26")
            else:
                self.Matrix.setRowCount(self.vertexCount)
                self.Matrix.setColumnCount(self.vertexCount)

                alph = ("A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z").split(",")
                self.VertexNames = alph[:self.vertexCount]
                self.Matrix.setHorizontalHeaderLabels(self.VertexNames)
                self.Matrix.setVerticalHeaderLabels(self.VertexNames)

                for i in range(self.vertexCount):
                    for j in range(self.vertexCount):
                        if i == j:
                            item = QTableWidgetItem()
                            item.setText("0")
                            self.Matrix.setItem(i, j, item)
                            item.setFlags(QtCore.Qt.ItemIsEnabled)
                            item.setBackground(QtGui.QColor(230, 230, 230))
                        else:
                            x = random.randint(0, 100)
                            self.Matrix.setItem(i, j, QTableWidgetItem(str(x)))

                self.Matrix.resizeColumnsToContents()
                self.statusbar.showMessage("Матрица успешно создана")
                self.matrixIsCreate = True

    def RunMethod(self):
        if self.matrixIsCreate is False:
            return
        if self.VertexNum.text() != "" and self.VertexNum.text() not in self.VertexNames:
            self.matrixModel = [[0] * self.vertexCount for i in range(self.vertexCount)]
            for i in range(self.vertexCount):
                for j in range(self.vertexCount):
                    self.matrixModel[i][j] = int(self.Matrix.item(i, j).text())

            if self.Methods.currentIndex() == 0:
                self.Protocol.appendPlainText("Алгоритм ближайшего соседа:")
                self.NearestNeighborAlgorithm()

            elif self.Methods.currentIndex() == 1:
                self.Protocol.appendPlainText("Алгоритма имитации отжига:")
                self.SimulatedAnnealing()

            elif self.Methods.currentIndex() == 2:
                self.Protocol.appendPlainText("Муравьинного алгоритм:")
                self.AntColony()
            self.statusbar.showMessage("Алгоритм завершил свою работу")

    def NearestNeighborAlgorithm(self):
        passVertex = []
        nextInd = None
        wayCost = 0

        curVertex = self.StartVertex.text()
        ind = self.VertexNames.index(curVertex)
        startVertexIndex = ind

        for j in range(self.vertexCount):
            minimum = None
            for i in range(self.vertexCount):
                if self.matrixModel[ind][i] > 0 and self.VertexNames[i] not in passVertex:
                    if minimum is None:
                        minimum = self.matrixModel[ind][i]
                        nextInd = i
                    elif minimum > self.matrixModel[ind][i]:
                        minimum = self.matrixModel[ind][i]
                        nextInd = i
            if minimum is None:
                if j == self.vertexCount - 1 and self.matrixModel[ind][startVertexIndex] > 0:
                    minimum = self.matrixModel[ind][startVertexIndex]
                    wayCost += minimum
                    passVertex.append(self.VertexNames[ind])
                    self.Protocol.appendPlainText("Кратчайший гамильтонов цикл " +
                                                  str(passVertex) + " равен: " + str(wayCost) + "\n")

                else:
                    self.Protocol.appendPlainText("Из данной точки не удалось построить гамильтонов цикл" + "\n")
                return
            else:
                passVertex.append(self.VertexNames[ind])
                ind = nextInd
                wayCost += minimum
        self.Protocol.appendPlainText(str(passVertex))
        self.statusbar.showMessage("Метод завершил работу")

    def SimulatedAnnealing(self):
        e = 2.71828182846  # експонента
        T = 100  # температура
        step = 0  # количество перемешивания пути для проверки его существования

        minT = int(self.MinTemperature.text())
        stepT = float(self.StepTemperature.text())

        if minT >= T or stepT >= T:  # проверка значений шага и минимальной температуры
            self.Protocol.appendPlainText("Значение шага и минимальной температуры должны быть меньше " + str(T) + "\n")
            return

        random.seed()
        path1 = list(range(self.vertexCount))  # список индексов пути
        random.shuffle(path1)

        while not self.isExist(path1) and step < 50:  # проверка на существования пути
            random.shuffle(path1)
            step += 1

        if step == 50:
            self.Protocol.appendPlainText("Не удалось построить гамильтонов цикл" + "\n")
            return

        pathLen = len(path1)
        pathByLetter = []  # список вершин в пути

        length1 = 0
        for i in range(self.vertexCount):  # подсчет длины первого пути
            length1 += self.matrixModel[path1[i - pathLen]][path1[i+1 - pathLen]]

        for i in range(pathLen):  # перевод списка индексов пути в список вершин
            pathByLetter.append(self.VertexNames[path1[i]])
        self.Protocol.appendPlainText("Начальный цикл: " + str(pathByLetter) + " за: " + str(length1))

        while T > minT:  # пока температура больше минимальной температуры
            firstind = 0
            secondind = 0
            while firstind == secondind:  # генерация двух случайных чисел
                firstind = random.randint(0, self.vertexCount-1)
                secondind = random.randint(0, self.vertexCount-1)

            path2 = path1.copy()  # инициализация второго пути с измененым (свап двух вершин) порядком вершин
            path2[firstind] = path1[secondind]
            path2[secondind] = path1[firstind]

            if not self.isExist(path2):  # если путь не существует, пропускаем итерацию
                continue

            length2 = 0
            for i in range(self.vertexCount):  # подсчет длины второго пути
                length2 += self.matrixModel[path2[i - pathLen]][path2[i+1 - pathLen]]

            if length2 < length1:  # если длина второго пути меньше, взять его вместо первого
                path1 = path2
                length1 = length2
            else:  # иначе, дать ему шанс стать первым (чем меньше температура, тем меньше шанс)
                p = 100*(e**(-(length2-length1)/T))
                x = random.randint(0, 100)
                if p > x:
                    path1 = path2
                    length1 = length2
            T -= stepT

        pathByLetter = []
        for i in range(pathLen):  # перевод списка индексов пути в список вершин
            pathByLetter.append(self.VertexNames[path1[i]])

        self.Protocol.appendPlainText("Кратчайший гамильтонов цикл: " + str(pathByLetter) + " за: " + str(length1) + "\n")

    def isExist(self, path):
        pathLen = len(path)
        for i in range(self.vertexCount):
            if self.matrixModel[path[i - pathLen]][path[i+1 - pathLen]] < 1:
                return False
        return True

    def AntColony(self):
        passVertex = None  # пройденные вершины
        passVertexInd = None  # индексы пройденных вершин
        length1 = None
        curVertex = self.StartVertex.text()  # текущая вершина
        ind = self.VertexNames.index(curVertex)  # индекс текущей вершины
        startVertexIndex = ind  # индекс стартовой вершины
        Q = int(self.Q.text())  # коэффициент увелечения силы феромонов
        Ants = int(self.Ants.text())  # кол-во муравьев (кол-во выполнений цикла)
        p = 0.4  # коэффициент забывания
        alpha = int(self.Alpha.text())  # параметр влияющий на жадность алгоритма
        beta = int(self.Beta.text())  # параметр влияющий на стадность алгоритма
        random.seed()

        self.Pheromone = [[0] * self.vertexCount for i in range(self.vertexCount)]  # матрица феромонов
        self.reverseMatrixModel = [[0] * self.vertexCount for i in range(self.vertexCount)]  # матрица обратных весов
        for i in range(self.vertexCount):  # инициализация феромонов и МОВ
            for j in range(self.vertexCount):
                if self.matrixModel[i][j] > 0:
                    self.reverseMatrixModel[i][j] = 1 / self.matrixModel[i][j]
                    self.Pheromone[i][j] = 1

        Way = []  # итоговый путь
        L = 0  # итоговая длина пути
        for l in range(Ants):
            passVertex = []
            passVertexInd = []
            length1 = 0
            ind = startVertexIndex
            success = False
            for k in range(self.vertexCount):
                if k == self.vertexCount - 1:
                    if self.matrixModel[ind][startVertexIndex] > 0:
                        length1 += self.matrixModel[ind][startVertexIndex]
                        passVertex.append(self.VertexNames[ind])
                        passVertexInd.append(ind)
                        Way = passVertex
                        L = length1
                        success = True
                        break
                    else:
                        break
                probability = []  # список вероятностей выбора

                for i in range(self.vertexCount):  # поиск вероятностей перехода из ind в другие вершины
                    if self.matrixModel[ind][i] <= 0 or self.VertexNames[i] in passVertex:
                        probability.append(0)
                    elif self.matrixModel[ind][i] > 0 and self.VertexNames[i] not in passVertex:
                        # sumOfMultiplyRMandP - сумма произведений обратной матрицы на феромон для всех возможных путей из вершины ind
                        p1 = 100 * (self.reverseMatrixModel[ind][i]**alpha * self.Pheromone[ind][i]**beta) / \
                             (self.sumOfMultiplyRMandP(ind, passVertex, alpha, beta))
                        probability.append(p1)

                x = random.randint(0, 99)
                j = 0
                p2 = 0
                flag = False
                for i in range(len(probability)):  # выбераем путь
                    p2 += probability[i]
                    if x - p2 < 0:
                        j = i
                        flag = True
                        break
                if flag is False:
                    break

                length1 += self.matrixModel[ind][j]
                passVertex.append(self.VertexNames[ind])
                passVertexInd.append(ind)
                ind = j

            if not success:
                break

            deltaPheromon = Q / (length1)
            for i in range(self.vertexCount):
                for j in range(self.vertexCount):
                    self.Pheromone[i][j] = self.Pheromone[i][j] * (1 - p)

            lenPassVertexInd = len(passVertexInd)
            for i in passVertexInd:
                self.Pheromone[i - lenPassVertexInd][passVertexInd[i + 1 - lenPassVertexInd]] += deltaPheromon
            #self.Pheromone[passVertexInd[-1]][passVertexInd[0]] += deltaPheromon

        if Way:
            self.Protocol.appendPlainText("Кратчайший гамильтонов цикл: " + str(Way) + " за: " + str(L) + "\n")
            #self.Protocol.appendPlainText(str(self.Pheromone))
        else:
            self.Protocol.appendPlainText("Из данной точки не удалось построить гамильтонов цикл" + "\n")

    def sumOfMultiplyRMandP(self, ind, passV, a, b):
        sum = 0
        for i in range(self.vertexCount):
            if self.matrixModel[ind][i] > 0 and self.VertexNames[i] not in passV:
                sum += (self.reverseMatrixModel[ind][i]**a * self.Pheromone[ind][i]**b)
        return sum

    def ClearProtocolMethod(self):
        self.Protocol.clear()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

