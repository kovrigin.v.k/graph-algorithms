## Description
The program finds the shortest route through all the vertices from the given vertex (solves the traveling salesman problem). It uses the following algorithms: “nearest neighbor algorithm”, “annealing simulation method”, and “ant algorithm”.

## Example
![picture](screenshots/GA1.png)